//ROUTES
weatherApp.config(function ($routeProvider) {
      
        $routeProvider
                             
        .when('/', {
            templateUrl: 'page/homepage.htm',
            controller: 'homeController'
        })
                  
        .when('/forecast', {
            templateUrl: 'page/forecast.htm',
            controller: 'forecastController'
        })
        
        .when('/forecast/:days', {
            templateUrl: 'page/forecast.htm',
            controller: 'forecastController'
        })
        
});